var ClientType = project.getClientType();

/* - - - - - - - - - - -  */
/* - - - - - - - - - - -  */
/* JAVASCRIPT PER RUNTIME */
/* - - - - - - - - - - -  */
/* - - - - - - - - - - -  */

if(ClientType=="local"){

    project.usbpath = "/mnt/usbmemory";
    function CheckUSB_OnAction(me, eventInfo)
    {
        var state = new State();
        var USBPlugged      = fs.exists("/mnt/usbmemory");
        var USB2Plugged     = fs.exists("/mnt/usbmemory2");
        if(USBPlugged){
            project.usbpath = "/mnt/usbmemory";
        }else{
            if(USB2Plugged){
                project.usbpath = "/mnt/usbmemory2";
            }else{
                project.usbpath = "/mnt/usbmemory";
            }
        }

        var PLCUpdSh        = fs.exists(project.usbpath+"/PlcUpd.sh") && ( fs.exists(project.usbpath+"/Application") || fs.exists(project.usbpath+"/naviopfiles") );
        var UpdPckOK        = fs.exists(project.usbpath+"/PLCUpdatePackage.zip");

        if((USBPlugged || USB2Plugged)){
            project.setTag("USBReady",1,0);
            if(PLCUpdSh){
                project.setTag("UpdatePLCReady",1,0);
            }
            if(UpdPckOK){
                project.setTag("PLCUpdatePackageReady",1,0);
            }
        }else{
            project.setTag("USBReady",0,0);
            project.setTag("UpdatePLCReady",0,0);
            project.setTag("PLCUpdatePackageReady",0,0);
        }
    }
}


/********************************************************************************************************************************************/

/* - - - - - - - - -  */
/* - - - - - - - - -  */
/* JAVASCRIPT PER WEB */
/* - - - - - - - - -  */
/* - - - - - - - - -  */

if(ClientType=="web"){
    
    
    console.log("Itizialization");
    
    var doingHash = false;  // questo è un semaforo: se clicco sul popup della pagina allarmi
    var state = new State();
    
    project.enableResize=1;
     
    
    // To avoid the OnPress freeze when lose focus
    function simulateClickOnLoseFocus()
    {
        function generateUpEvent()
        {
            $hmi.mouseMgr.loseFocus();
        }
        window.addEventListener('blur', generateUpEvent, false);
        window.addEventListener('focus', generateUpEvent, false);
    }   
    
    simulateClickOnLoseFocus(); 
    

    document.addEventListener('keydown', function(event){
        if (event.code === "ArrowLeft")
           project.prevPage();
        else if (event.code === "ArrowRight")
           project.nextPage();
    } );
    
    
    // Get PageMgr widget
    var pageMgr = project.getWidget( "_PageMgr" );   
    console.log("START")
    for (var a in pageMgr){
        console.log(pageMgr[a]);
    }
    console.log("END");
    console.log("INIT - current url: " + window.location.href);
    
    
    //-------------------------------------------------------------------
    //---------------------------- PRECACHE -----------------------------
    //-------------------------------------------------------------------
    
	//CACHING - Available from ND 2.8
    //project.setProperty( "pagesCacheSize", 10 ) ;
    //lo faccio già quando ho finito il pracachin //project.changePageAnimation(true); // adding animates spinner on page changing
     project.setProperty( "changePageAnimationOnCache", false ); // disable spinner if new page has been already cached
    
    $hmi.limitPages = 10;
	
	project.homeFull         = "engine_engines1_day_full";
	project.homeHalf         = "engine_engines1_day_half";
	//project.homeRow        = "engine_nav-engine1_day_row"; //OPT
	//project.homeCol        = "engine_nav-engine1_day_col"; //OPT
	project.comErrorPageName = "engine_comerror_day";
    project.comErrorFull     = "engine_comerror_day_full";
    
    //scelta pagine da cacheare
    var width = window.innerWidth;
    var height = window.innerHeight;
    var viewportRatio=width/height;
    
	// Le scelgo direttamente senza inserire il check pagine half / full /colonna


    
    /*if(viewportRatio<0.5 && project.Video==1){   //Column
        console.log("Precache - array column");
        var precachedPages=["control1_col"];
        
        //in questo caso il precache finisce subito. C'è comqunque un brutto effetto
        //dovuto all'immagine Naviop al centro del waiting div che viene tagliata. Whatever
     }
     else*/ if(viewportRatio<1 ){     //Half
        console.log("Precache - array half");
		var precachedPages=["engine_comerror_day_half",
                            "engine_engines1_day_half", "engine_alarms_day_half",
                            "engine_comerror_day_full", "engine_comerror_day_half"  //le metto sempre in cache
                ];
     }
     else if(viewportRatio<3 ){     //Full
        console.log("Precache - array full");
		var precachedPages=["engine_comerror_day_full",
							"engine_engines1_day_full", "engine_service_day_full", "engine_alarms_day_full", 
                            "engine_comerror_day_full", "engine_comerror_day_half",  //le metto sempre in cache
                ];			
     }
    
   


    var percacheIndex=0;
    var waitingDiv;
    var progressBar;
    var progressBarFrame;           
    var precachedComplete = false;
    
    project.precachePages=function fun(){
            console.log("Precache - inizio precache");
            percacheIndex=0;
            precachedComplete = false;
            
            //Copro con un DIV per coprire i cambi pagina       
            waitingDiv = document.createElement('div');
            waitingDiv.style.display = "block";
            waitingDiv.style.visibility = "visible";
            waitingDiv.style.position = "absolute";
            waitingDiv.style.left = "0";
            waitingDiv.style.top = "0";
            waitingDiv.style.height = "100%";
            waitingDiv.style.width = "100%";
            waitingDiv.style.background="black";
            waitingDiv.style.overflow="hidden";
            waitingDiv.style.textAlign="center";
            waitingDiv.style.verticalAlign="middle";
            waitingDiv.scrolling="no"
            
            var txtDiv = document.createElement('div');
            txtDiv.style.display = "block";
            txtDiv.style.visibility = "visible";
            txtDiv.style.position = "absolute";
            txtDiv.style.left = "0%";
            txtDiv.style.top = "90%";
            txtDiv.style.width = "100%";
            txtDiv.style.textAlign="center";                 
            var par = document.createElement("P");
            var txt1 = document.createTextNode('Loading');   
            par.appendChild(txt1);
            par.style.fontSize = "xx-large"; 
            par.style.fontFamily = "sans-serif"; 
            par.style.color = "white";
            txtDiv.appendChild(par); 
            waitingDiv.appendChild(txtDiv);
            
            
    /*
            var img=document.createElement("img");
            img.style.position="absolute";
            img.src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iCiAgIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiCiAgIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIKICAgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB2ZXJzaW9uPSIxLjEiCiAgIHdpZHRoPSIzMyIKICAgaGVpZ2h0PSIzMyIKICAgdmlld0JveD0iMCAwIDMzIDMzIgogICBpZD0iTGl2ZWxsb18xIgogICB4bWw6c3BhY2U9InByZXNlcnZlIj48bWV0YWRhdGEKICAgaWQ9Im1ldGFkYXRhMzIiPjxyZGY6UkRGPjxjYzpXb3JrCiAgICAgICByZGY6YWJvdXQ9IiI+PGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+PGRjOnR5cGUKICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz48ZGM6dGl0bGU+PC9kYzp0aXRsZT48L2NjOldvcms+PC9yZGY6UkRGPjwvbWV0YWRhdGE+PGRlZnMKICAgaWQ9ImRlZnMzMCI+CgkKCQo8L2RlZnM+Cgo8ZwogICB0cmFuc2Zvcm09Im1hdHJpeCgwLjg0ODQ4NDgsMCwwLDAuODQ4NDg0OCwyMC4yMDIyNjIsNC42NjM1MzM5KSIKICAgaWQ9ImczMTMzIj48ZwogICAgIHRyYW5zZm9ybT0ibWF0cml4KDAuMDc3NTEwMjgsMCwwLDAuMDc3NTEwMjgsLTIwLjg2MzM4MSwtMi41NDk4NzkzKSIKICAgICBpZD0iZzUiCiAgICAgc3R5bGU9ImZpbGw6I2ZmZmZmZiI+PHBhdGgKICAgICAgIGQ9Ik0gMjEyLjg3NSw0MjUuNzUgQyA5NS40OTQsNDI1Ljc1IDAsMzMwLjI1NiAwLDIxMi44NzUgMCw5NS40OTQgOTUuNDk0LDAgMjEyLjg3NSwwIDMzMC4yNTYsMCA0MjUuNzUsOTUuNDk0IDQyNS43NSwyMTIuODc1IGMgMCwxMTcuMzgxIC05NS40OTQsMjEyLjg3NSAtMjEyLjg3NSwyMTIuODc1IHogbSAwLC0zNTQuMDU5IGMgLTc3Ljg0OCwwIC0xNDEuMTg1LDYzLjMzNSAtMTQxLjE4NSwxNDEuMTg0IDAsNzcuODQ5IDYzLjMzNiwxNDEuMTg1IDE0MS4xODUsMTQxLjE4NSA3Ny44NDgsMCAxNDEuMTg0LC02My4zMzYgMTQxLjE4NCwtMTQxLjE4NSAwLC03Ny44NDkgLTYzLjMzNiwtMTQxLjE4NCAtMTQxLjE4NCwtMTQxLjE4NCB6IgogICAgICAgaWQ9InBhdGg3IgogICAgICAgc3R5bGU9ImZpbGw6I2ZmZmZmZiIgLz48L2c+PHBhdGgKICAgICBkPSJtIC00LjY4NDExODUsOC4zMTI0OTA5IGMgLTEuNTM0MzkzLDAgLTIuNzc4NDMzLDEuMjQzODggLTIuNzc4NDMzLDIuNzc4NDMwMSAwLDAuNDQxOTYgMCw1LjI0MTQgMCw1LjcxODQgMCwxLjUzNDQ3IDEuMjQ0MDQsMi43NzgzNSAyLjc3ODQzMywyLjc3ODM1IDEuNTM0Mzk0LDAgMi43NzgzNTYsLTEuMjQzOCAyLjc3ODM1NiwtMi43NzgzNSAwLC0wLjQwOTE4IDAsLTUuMjE2MjEgMCwtNS43MTg0IDAsLTEuNTM0NTUwMSAtMS4yNDM5NjIsLTIuNzc4NDMwMSAtMi43NzgzNTYsLTIuNzc4NDMwMSB6IgogICAgIGlkPSJwYXRoOSIKICAgICBzdHlsZT0iZmlsbDojZmZmZmZmIiAvPjwvZz48L3N2Zz4="
            img.style.height = "50%";
            //img.style.width = "50%";
            img.style.top = "0";
            img.style.top = "0";
            img.style.left = "0";
            img.style.right = "0";
            img.style.bottom = "0";
            img.style.margin = "auto";
            img.style.overflow = "auto";
            
            waitingDiv.appendChild(img);
        */    
      /*      
            var txtDiv1 = document.createElement('div');
            txtDiv1.style.display = "block";
            txtDiv1.style.visibility = "visible";
            txtDiv1.style.position = "absolute";
            txtDiv1.style.left = "0%";
            txtDiv1.style.top = "50%";
            txtDiv1.style.width = "50%";
            txtDiv1.style.textAlign="center";                 
            var par1 = document.createElement("P");
            var txt2 = document.createTextNode('WARNING THIS ONBOARD MONITORING AND CONTROL SYSTEM IS DESIGNED TO FUNCTION AS AN AUXILLARY SYSTEM.THIS DATA IS DESIGNED TO AID TRADITIONAL NAVIGATION METHODS, NOT AS A REPLACEMENT FOR SOUND NAVIGATIONAL JUDGMENT.DO NOT RELY ON THIS PRODUCT AS A PRIMARY SOURCE OF NAVIGATION. THE OPERATOR IS RESPONSIBLE FOR USING OFFICIAL GOVERNMENT CHARTS AND PRUDENT METHODS FOR SAFE NAVIGATION WHEN USING THIS PRODUCT. THIS DATA MAY CHANGE WITHOUT PRIOR NOTICE AND MAY NOT BE THE LATEST UPDATE. THIS DISCLAIMER APPLIES BOTH TO INDIVIDUAL USE OF THE DATA AND AGGREGATE USE WITH OTHER DATA.');   
            par1.appendChild(txt2);
            par1.style.fontSize = "xx-large"; 
            par1.style.fontFamily = "sans-serif"; 
            par1.style.color = "white";
            txtDiv1.appendChild(par1); 
            waitingDiv.appendChild(txtDiv1);
      */      
            
            progressBarFrame = document.createElement('div');
            progressBarFrame.style.display = "block";
            progressBarFrame.style.visibility = "visible";
            progressBarFrame.style.position = "absolute";
            progressBarFrame.style.left = "40%";
            //progressBarFrame.style.top = "85%";
            progressBarFrame.style.top = "45%";
            progressBarFrame.style.height = "20px";
            progressBarFrame.style.width = "20%";
            progressBarFrame.style.background="#919191";
            progressBarFrame.style.overflow="hidden";
            progressBarFrame.style.textAlign="center";
            progressBarFrame.style.verticalAlign="middle";
            progressBarFrame.style.borderRadius="16px";
            progressBarFrame.scrolling="no"
            
            progressBar = document.createElement('div');
            progressBar.style.display = "block";
            progressBar.style.visibility = "visible";
            progressBar.style.position = "absolute";
            progressBar.style.left = "0%";
            progressBar.style.top = "0%";
            progressBar.style.height = "100%";
            progressBar.style.width = "0%";
            progressBar.style.background="white";
            progressBar.style.overflow="hidden";
            progressBar.style.textAlign="center";
            progressBar.style.verticalAlign="middle";
            progressBar.style.borderRadius="16px";
            progressBar.scrolling="no"
             
            progressBarFrame.appendChild(progressBar);
            
            waitingDiv.appendChild(progressBarFrame);
            console.log("Precache - Mostro il DIV");
            document.body.appendChild(waitingDiv);
            
            //Faccio partire il cambio pagina ricorsivo
            
            // No more valid
            //var container = document.getElementById( "container" );
            //$hmi.getEventMgr().on( container, "hmiafterchange", precacheCallback); 
           // precacheCallback();
       
            // ------------ Update to workaround --------------
            // Align naviop workaround with jm4web precache flow     
            project.on( "precacheend", function() {
                var container = document.getElementById( "jm-container" ); // Update to workaround
                $hmi.getEventMgr().on( container, "hmiafterchange",precacheCallback);
                precacheCallback();
            } );
            wrCheckAndTriggerWithSlowNetwork();
            // -----------------------------------------------
     
        }
        
        // updated
        function wrCheckAndTriggerWithSlowNetwork()
        {
            var pageMgr = project.getWidget( "_PageMgr" );
            if ( !pageMgr.wgt.isPrecaching() && pageMgr.wgt.waitPrecacheEnd() != null )
            {
                project.wgt.triggerEvent( "precacheend" );
            }  
        }
    
    function precacheCallback(){    
         var targetCachePage = precachedPages[percacheIndex];
         var currPage = pageMgr.getCurrentPageName();
         console.log("Precache - Current page: "+pageMgr.getCurrentPageName());
         console.log("Precache - Caching page: "+targetCachePage);
         console.log("Precache - loading: "+Math.floor(percacheIndex/precachedPages.length*100)+"%");
         console.log("Precache - loading: "+percacheIndex+"/"+precachedPages.length);
         setLoadingPercentage(percacheIndex/precachedPages.length*100);
         
         if ( targetCachePage != undefined ) {
             var dontSkip= ((targetCachePage != currPage)) ;
             var sizeCurr= currPage.substring(currPage.length-5);
             var sizeTrgt= targetCachePage.substring(targetCachePage.length-5);
             dontSkip= dontSkip && (sizeCurr == sizeTrgt || currPage == project.objectName);
             console.log("Precache - Current page size: "+sizeCurr);
             console.log("Precache - Caching page size: "+sizeTrgt);
             
             if( dontSkip ){
                 console.log("Precache - cambio pagina: "+targetCachePage);
                 project.loadPage(targetCachePage);
                 percacheIndex++;
             }else{
                 console.log("Precache - skipped page: "+targetCachePage); 
                 percacheIndex++;
                 precacheCallback();
             }
         } else {
             if ( percacheIndex <= precachedPages.length ) {
                 //hideLoadingbar();
                 project.loadPage(project.homeFull);
                 //project.changePageAnimation(true); // adding animates spinner on page changing
                 project.setProperty( "changePageAnimationOnCache", false ); // disable spinner if new page has been already cached
                 setTimeout(function(){              
                     if ( document.body.contains( waitingDiv ) ){
                         document.body.removeChild(waitingDiv);
                         // ------------ Update to workaround --------------
                         var container = document.getElementById( "jm-container" ); 
                         $hmi.getEventMgr().off( container, "hmiafterchange",precacheCallback);
                         // -------------------------------------------------
                     }
                 }, 500 );
                 percacheIndex++;
             }
         }
     } 
    
    function hideLoadingbar(){
        if ( waitingDiv.contains( progressBarFrame ) ){
            waitingDiv.removeChild(progressBarFrame); 
        }
    }
    
    function setLoadingPercentage(perc){
        progressBar.style.width = ""+Math.floor(perc)+"%";
    }
    
    
    
    project.precachePages();   
    

   
    /************** GESTIONE CAMBI PAGINA E RESIZE  ****************/
    
    function new_resize(eventInfo){
        
        console.log(doingHash);
        
        if (doingHash){
            console.log("Loading Hash page - preventing resize event")
            doingHash = false;
            return;
        }
        
        
        var width = eventInfo.width;
        var height = eventInfo.height;
        var viewportRatio=width/height;
        var currentPage=pageMgr.getCurrentPageName(); 
        
        var FullIdx=currentPage.indexOf("_full"); // se non trova la sottostringa, restituisce -1
        var HalfIdx=currentPage.indexOf("_half"); // se non trova la sottostringa, restituisce -1
        var ColIdx=currentPage.indexOf("_col"); // se non trova la sottostringa, restituisce -1
        var RowIdx=currentPage.indexOf("_row"); // se non trova la sottostringa, restituisce -1
        var pageName = "home";
        if(FullIdx>0){
          pageName = currentPage.slice(0,FullIdx) ;
        }
        if(HalfIdx>0){
          pageName = currentPage.slice(0,HalfIdx) ;
        }
        if(ColIdx>0){
          pageName = currentPage.slice(0,ColIdx) ;
        }
        if(RowIdx>0){
          pageName = currentPage.slice(0,RowIdx) ;
        }
    
        var targetPage = newTargetPage(viewportRatio, pageName);
    
        if (currentPage==targetPage){
            console.log("resizing");
            return null;                    //se la pagina di arrivo è quella giusta faccio il resize.
        }else{
            console.log("changing page");
            project.loadPage(targetPage);
            return false;                   //altrimenti cambio pagina e blocco il resize.
        }
        
    }
    
    function new_pageChange(eventInfo){
        var currentPage = eventInfo.src;
        var destinationPage = eventInfo.dst;
        var width = eventInfo.width;
        var height = eventInfo.height;
        var viewportRatio=width/height;
        
        if (destinationPage!=null)
        {
            var FullIdx=destinationPage.indexOf("_full"); // se non trova la sottostringa, restituisce -1
            var HalfIdx=destinationPage.indexOf("_half"); // se non trova la sottostringa, restituisce -1
            var ColIdx=destinationPage.indexOf("_col"); // se non trova la sottostringa, restituisce -1
            var RowIdx=destinationPage.indexOf("_row"); // se non trova la sottostringa, restituisce -1
         }else{
            var FullIdx= -1;
            var HalfIdx= -1;
            var ColIdx= -1;
            var RowIdx= -1;    
         }
         var pageName = "home";    
           

        if(FullIdx>0){
          pageName = destinationPage.slice(0,FullIdx) ;
        }
        if(HalfIdx>0){
          pageName = destinationPage.slice(0,HalfIdx) ;
        }
        if(ColIdx>0){
          pageName = destinationPage.slice(0,ColIdx) ;
        }
        if(RowIdx>0){
          pageName = destinationPage.slice(0,RowIdx) ;
        }

        
        console.log("viewportRatio: "+viewportRatio+" pagina: "+pageName);
        var targetPage = newTargetPage(viewportRatio, pageName);
        console.log("Nuova pagina: "+targetPage);
        console.log("Destination page: "+destinationPage);
        /*
        if (destinationPage.indexOf("_day") > 0){
            project.setDay();
        }
        else if (destinationPage.indexOf("_hc") > 0){
            project.setHc();
        }
        */
        
        if (doingHash){
            doingHash = false;  // azzero il semaforo dell'Hash
            console.log("Finished loaging Hash page");
        }    
     
        if(destinationPage==targetPage){
            console.log("Confirmig new page");
            return null;                    //se sto andando alla pagina giusta, lascio fare
        }else{
            console.log("redirecting to: "+targetPage);
            return targetPage;              //altrimenti impongo quella nuova
        }  
    }
    
    function newTargetPage(viewportRatio,pageName){
      /* if(viewportRatio<0.5){          //Column
            if( pageMgr.hasPage(pageName+"_col")){
                return pageName+"_col";
            }else{
                return "control1_col";  
            }
        }
        else */ if (viewportRatio < 1 ){     //Half
            if( pageMgr.hasPage(pageName+"_half")){
                return pageName+"_half";
            }else{
                return project.homeHalf;
            }
        }
        else if(viewportRatio<3 ){     //Full
            if( pageMgr.hasPage(pageName+"_full")){
                return pageName+"_full";
            }else{
                return project.homeFull;
            }
        }
     /*   else if(viewportRatio>=3){      //Row
            if( pageMgr.hasPage(pageName+"_row")){
                return pageName+"_row";
            }else{
                return "home_row";
            }
        } */
        else{
            return project.homeFull; //Default
        }
    }  


    
	/************* Controllo Connettività Naviop	*******/
	//Timout prima di iniziare il controllo
	var timeoutForCheckFunction=90000;
	var periodForCheckFunction=5000;
	project.protocolErrorInterval=null;

	//rifaccio tutto. Timeout di 60 secondi. Dopo 60 secondi inizio a
	//fare il check a prescindere e non lo tocca più nessuno. Non viene interrotto o fatto ripartire

	var state = new State();
	var isInitialized=project.getTag( "/JMOBGV/SysInizialized_HMI", state, -1, 1);

	if(isInitialized){
		//window.clearInterval(project.protocolErrorInterval);
		project.protocolErrorInterval=window.setInterval(function(){
				checkPLCCommError();
		}, periodForCheckFunction);
	}else{
		window.setTimeout(function(){
						//window.clearInterval(project.protocolErrorInterval);
						project.protocolErrorInterval=window.setInterval(function(){
								checkPLCCommError();
						}, periodForCheckFunction);
		}, timeoutForCheckFunction);
	}



	function checkPLCCommError(){
		try{
			var statePLC = new State();
			var stateVB = new State();
			project.getTag("Application/GVL_ExportHMI/PLC_ComCheck_HMI",statePLC,-1,function(tagName, state){
				if (statePLC.getValue()!=1){
					project.ComErrorCountPLC=project.ComErrorCountPLC+1;
                    console.log("project.ComErrorCountPLC: " + project.ComErrorCountPLC);
				}else{
					project.ComErrorCountPLC=0;
                    console.log("RESET project.ComErrorCountPLC: " + project.ComErrorCountPLC);
				}
				if(project.ComErrorCountPLC>12 || project.ComErrorCountVBOX>12){
					project.loadPage(project.comErrorFull); 
                    console.log("ComError: loading comError page");
				}else{
					currentPage=pageMgr.getCurrentPageName();
                    //if(currentPage==project.comErrorFull || currentPage==project.comErrorHalf){ 
                    if( currentPage.indexOf(project.comErrorPageName) > -1 ){ 
						project.loadPage(project.homeFull);
                        console.log("ComError: loading home page");
					}
				}
                console.log("ComError: setting ComCheck to 0");
			} , false );
		}catch(e){
			console.log("Errore getTag per dialog "+e);
		}
	}
    
    
    //-------------------------------------------------------------------
    //----------------------CAMBIO PAGINA FRAMENT------------------------
    //-------------------------------------------------------------------
    //TODO: 
    //Flavio -> non cambiare window.location ogni volta che cambio pagina. Si può fare override?
    //Flavio -> utilizzare ash anche al posto di Alias ed evitare loading.html?loadpage=pagina1 
    //Navico -> togliere parametri da url
    //Navico -> implementare CAL con hash anziché con 
    
    
    window.addEventListener("hashchange", HashChanged);
    //console.log('project:' + window.location.href);
    
    function HashChanged(){
        console.log('New location:' + window.location.href);
        var newPage= location.hash;
        newPage=newPage.replace('#','');
        console.log('Hash value:' + newPage);
        doingHash = true;
        project.loadPage(newPage+'.jmx');
    }    
    
/******** LISTENERS **********/   
try{
    project.on( "resize", new_resize );     
    project.on( "shouldChangePage", new_pageChange ); 
   

    // nei progetti Ferretti non si usa la rotella
    //document.addEventListener("DOMMouseScroll", MouseWheelHandler, false);
    //document.addEventListener("mousewheel", MouseWheelHandler, false);
    //window.addEventListener("keydown", dealWithKeyboard, false);

}catch(e){
    console.log("Error: "+e);
}    
    console.log("Init Done");    
} // ClientType






/********* BACKGROUND COLOR *********/
/*
project.setDay = function(){
   setBackgroundColor("rgb(10,27,38)") 
}

project.setHc = function(){
   setBackgroundColor("rgb(255,255,255)")
}

function setBackgroundColor(colorString){
    var cont = document.getElementById("jm-container");
    cont.style["background-color"] = colorString;
}
*/