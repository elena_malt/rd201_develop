@echo off

REM Remove _all_ .jmxb files in %projectRoot% and subfolders.
REM Usage: clearNDProjectBinaries.bat path:\\to\project\
REM 	   or drag project folder into clearNDProjectBinaries.bat

REM set "projectRoot=%1"
REM echo %projectRoot%
set "projectRoot=%~f1"
echo %projectRoot%

pushd "%projectRoot%"
REM echo %cd%

del /S /Q *.jmxb
echo Deleting opt and web folders...
RD /S /Q "opt"
RD /S /Q "web"
RD /S /Q "json"

del config\*.bin
del config\*.bak

popd

pause

