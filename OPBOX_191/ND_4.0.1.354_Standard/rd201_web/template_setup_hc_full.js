///function field1_onDataUpdate(me, eventInfo)
///  {
///    var id = eventInfo.newValue;
///        project.setLanguage( id );
///     return false;
///  }

// Memorizza pagina corrente e carica pagina allarmi per evitare circolo di lastVisitedPage()
function gotoAlarmsPage()
{
    try {
        var pageMgr = project.getWidget( "_PageMgr" );
        project.rememberedPage = pageMgr.getCurrentPageName();
        project.loadPage("engine_alarms_hc_full");
    } catch(e) {
        alert("gotoAlarmsPage - Error: " + e);
    }
}

function BottomMenu_Alarm_Btn_Left_btn_onMouseUp(me, eventInfo)
{
    gotoAlarmsPage();
}

function BottomMenu_Alarm_Btn_Right_btn_onMouseUp(me, eventInfo)
{
    gotoAlarmsPage();
}