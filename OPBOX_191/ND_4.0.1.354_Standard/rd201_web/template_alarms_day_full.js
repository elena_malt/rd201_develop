///function field1_onDataUpdate(me, eventInfo)
///  {
///    var id = eventInfo.newValue;
///        project.setLanguage( id );
///     return false;
///  }

// Per evitare circolo di LastVisitedPage.
function gotoRememberedPage() {
    if( !project.rememberedPage ){
        project.rememberedPage = project.homeFull;
    }
    if( project.rememberedPage ){
        project.loadPage(project.rememberedPage);
        project.rememberedPage = "";
    } else {
        project.lastVisitedPage();
    }
}

function BottomMenu_Engines1_Btn_Back_btn_onMouseUp(me, eventInfo)
{
    gotoRememberedPage();
}
